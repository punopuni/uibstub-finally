<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Аутентификация
Route::get('/', 'AuthController@index')->name('login');
Route::post('/auth', 'AuthController@authorization');
Route::get('/logout', 'AuthController@logout')->name('logout');


//Группировка авторизированных онли
Route::group([
    'middleware' => ['auth']
], function () {
//Личный кабинет
    Route::get('/personal_area', 'PersonalAreaController@index')->name('personal_area');
    Route::post('/upload_photo', 'PersonalAreaController@upload_photo')->name('upload_photo');

//Преподаватели
    Route::get('/teacher', 'TeacherController@index');
    Route::get('/teacher/{id}', 'TeacherController@category_index');
    Route::get('/teacher_article/{id}', 'TeacherController@teacher_article');

//Карта универа
    Route::get('/map', 'MapController@index');
    Route::get('/map_floor/{id}', 'MapController@floor_index');

//Новости
    Route::get('/news', 'NewsController@index');
    Route::get('/news/{id}', 'NewsController@category_index');

//Нетворк
    Route::get('/network', 'NewsController@index');
    Route::get('/network/{id}', 'NewsController@category_index');

//Создание нетворка
    Route::get('/network_form', 'NewsController@network_form');
    Route::post('/network_form', 'NewsController@network_save');

//Статьи нетворка и новостей
    Route::get('/article/{id}', 'NewsController@article');

//Потеряшки
    Route::get('/thinks_form', 'ThinkController@create');
    Route::post('/thinks_form', 'ThinkController@store');
    Route::get('/things', 'ThinkController@index');
    Route::get('/thing/{id}', 'ThinkController@category_index');
    Route::get('/thing_article/{id}', 'ThinkController@article_index');

//Рассписание
    Route::get('/schedule', 'ScheduleController@index');

});


//Админка
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/migrations/{key}', function ($key){
    $resultCode = Artisan::call('migrate');
    $results = [
        [
            'code'  => $resultCode,
            'result' => Artisan::output()
        ]
    ];
    $exitCode = Artisan::call('storage:link');
    $results[] = [
        'code' => $exitCode,
        'result' => Artisan::output()
    ];

    return response()->json($results);
})->where(['key' => 'WfQ7MvDm']);

@extends('layouts.app')
@section('content')
<body class="networking-form">
<div class="content">
    @include('layouts.main_menu')
    <div class="container">
        <div class="employees__title">Нетворкинг</div>
        <div class="things__form--wrapper">
            <p><span class="label-required">*</span>- обязательные поля</p>
            <form action="/network_form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="select">Выберите категорию нетворкинга<span class="label-required">*</span></label>
                    <select class="custom-select" id="select" name="category">
                        @foreach($categories as $cat)
                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Заголовок<span class="label-required">*</span></label>
                    <input class="form-control" id="title"  type="text" placeholder="Заголовок" name="name">
{{--                    <span class="form-error show"></span>--}}
                </div>
                <div class="form-group">
                    <label for="title">Краткое описание<span class="label-required">*</span></label>
                    <input class="form-control" id="title" type="text" placeholder="Заголовок" name="disc">
{{--                    <span class="form-error show"></span>--}}
                </div>
                <div class="form-group">
                    <label for="search-textarea">Описание (Ваша цель, кого ищете, в чем ваша идея и т.д.)
                        <span class="label-required">*</span>
                    </label>
                    <textarea class="form-control" id="search-textarea" rows="3" required="required" name="text"></textarea>
                </div>
                <div class="form-group">
                    <label for="search-file">Картинка</label>
                    <input class="form-control-file" id="search-file" type="file" name="photo">
                </div>
                <button class="btn btn--default" type="submit">Отправить</button>
            </form>
        </div>
    </div>
</div>
@endsection

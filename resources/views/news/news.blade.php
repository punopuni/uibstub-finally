@extends('layouts.app')
@section('content')
    <body class="news">
    <div class="content">
        @include('layouts.main_menu')
        <div class="container">
            @if($type == 1)
            <div class="employees__title">Новости</div>
            @else
            <div class="employees__title">Нетворгинг</div>
            @endif
            <div class="links">
                <div class="links__wrapper">
                    @if($type == 1)
                    <a class="links__item active" href="/news/">Все</a>
                    @foreach($categories as $cat)
                    <a class="links__item" href="/news/{{$cat->id}}">{{$cat->name}}</a>
                    @endforeach
                    @else
                    <a class="links__item active" href="/network/">Все</a>
                    @foreach($categories as $cat)
                        <a class="links__item" href="/network/{{$cat->id}}">{{$cat->name}}</a>
                    @endforeach
                    @endif
                </div>
            </div>
            <div class="employees__wrapper">
                @foreach($news as $item)
                <a class="employees__items" href="/article/{{ $item->id }}">
                    <div class="employees__items--bg">
                        @if($item->photo)
                        <div class="employees__img" style="background-image: url({{ asset('/storage/' . $item->photo) }})"></div>
                        @else
                        <div class="employees__img" style="background-image: url({{ asset('/img/no-photo.png') }})"></div>
                        @endif
                        <div class="news__wrapper">
                            <div class="news__date">{{$item->created_at}}</div>
                            <div class="news__author">{{ $item->user->name }}</div>
                        </div>
                        <div class="employees__name">{{$item->name}}</div>
                        <div class="employees__desc">{{$item->disc}}</div>
                    </div>
                </a>
                @endforeach
              {{ $news->links() }}
            </div>
        </div>
    </div>
@endsection

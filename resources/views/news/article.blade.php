@extends('layouts.app')
@section('content')
<body class="news-article">
<div class="content">
    <div class="container">
        @include('layouts.main_menu')
        <div class="personal-area__wrapper">
            <div class="news-article__wrapper">
                <h1 class="news-article__title">{{ $article->name }}</h1>
                <div class="news-article__date">{{ $article->created_at }}</div>
                @if($type != 1)
                    <a class="networking__link" href="/network/{{ $category->id }}">{{ $category->name }}</a>
                @endif
                <h6 class="personal-area__name">{{ $user->name }}</h6>
                <div class="news-article__img--wrapper">
                    <img class="news-article__img" src="{{ asset('/storage/' . $article->photo) }}">
                    <div class="personal-area__info">
                        {!! $article->text !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

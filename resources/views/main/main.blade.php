@extends('layouts.app')
@section('content')
<body class="personal-area">
    <div class="content">
        <nav class="main-menu">
            <div class="main-menu__wrapper">
                <ul>
                    <li><a href="index.html"><i class="fa fa-2x fas fa-user-graduate"></i><span class="nav-text">Личный кабинет</span></a></li>
                    <li class="has-subnav"><a href="employees.html"><i class="fa fa-2x fas fa-id-card-alt"></i><span class="nav-text">Преподователи</span></a></li>
                    <li class="has-subnav"><a href="map.html"><i class="fa fa-2x fas fa-map-marked-alt"></i><span class="nav-text">Карта ВУЗа</span></a></li>
                    <li class="has-subnav"><a href="news.html"><i class="fa fa-2x far fa-newspaper"></i><span class="nav-text">Новости</span></a></li>
                    <li><a href="#"><i class="fa fa-2x fas fa-book-open"></i><span class="nav-text">Расписание</span></a></li>
                    <li><a href="networking.html"><i class="fa fa-2x fas fa-users"></i><span class="nav-text">Нетворкинг</span></a></li>
                    <li><a href="things.html"><i class="fa fa-2x fas fa-search"></i><span class="nav-text">Потеряшка</span></a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="personal-area__wrapper">
                <div class="personal-area__item">
                    <div class="personal-area__ava--wrapper">
                        <div class="personal-area__ava" style="background-image: url(&quot;/img/ava.jpg&quot;)"></div><a class="personal-area__ava--edit" href="#">Редактировать<i class="fas fa-pen"></i></a>
                    </div>
                    <div class="personal-area__text">
                        <h6 class="personal-area__name">Левкович Дарья Андреевна</h6>
                        <div class="personal-area__info">
                            <p>Группа: ИС 16.309 РО</p>
                            <p>Форма: Очное</p>
                            <p>Уровень: Бакалавр</p>
                            <p>Специальность или ОП: Информационные системы</p>
                            <p>Штрих-код: 1610232</p>
                            <p>Курс: 4</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<nav class="main-menu">
    <div class="main-menu__wrapper">
        <ul>
            <li><a href="/personal_area"><i class="fa fa-2x fas fa-user-graduate"></i><span class="nav-text">Личный кабинет</span></a></li>
            <li class="has-subnav"><a href="/teacher"><i class="fa fa-2x fas fa-id-card-alt"></i><span class="nav-text">Преподователи</span></a></li>
            <li class="has-subnav"><a href="/map"><i class="fa fa-2x fas fa-map-marked-alt"></i><span class="nav-text">Карта ВУЗа</span></a></li>
            <li class="has-subnav"><a href="/news"><i class="fa fa-2x far fa-newspaper"></i><span class="nav-text">Новости</span></a></li>
            <li><a href="/schedule"><i class="fa fa-2x fas fa-book-open"></i><span class="nav-text">Расписание</span></a></li>
            <li><a href="/network"><i class="fa fa-2x fas fa-users"></i><span class="nav-text">Нетворкинг</span></a></li>
            <li><a href="/network_form"><i class="fa fa-2x fas fa-plus"></i><span class="nav-text">Нетворкинг - заявление</span></a></li>
            <li><a href="/thinks_form"><i class="fa fa-2x fas fa-search"></i><span class="nav-text">Потеряшка - заявление</span></a></li>
            <li><a href="/things"><i class="fa fa-2x fas fa-clipboard-list"></i><span class="nav-text">Списки потеряшек</span></a></li>
        </ul>
    </div>
</nav>

<footer class="footer">
    <div class="container">
        <div class="footer__wrapper">
            <div class="footer__item--icon">
                <a class="footer__icon" href="{{ setting('site.uib') }}">
                    <div class="footer__icon--img" style="background-image: url(&quot;/img/icons/uib.png&quot;)" title="UIB"></div>
                </a>
                <a class="footer__icon" href="{{ setting('site.moodle') }}">
                    <div class="footer__icon--img" style="background-image: url(&quot;/img/icons/favicon.png&quot;)" title="moodle"></div>
                </a>
                <a class="footer__icon" href="{{ setting('site.insta') }}">
                    <div class="footer__icon--img" style="background-image: url(&quot;/img/icons/instagram.png&quot;)" title="instagram"></div>
                </a>
                <a class="footer__icon" href="{{ setting('site.vk') }}">
                    <div class="footer__icon--img" style="background-image: url(&quot;/img/icons/vk.png&quot;)" title="vk"></div>
                </a>
                <a class="footer__icon" href="{{ setting('site.facebook') }}">
                    <div class="footer__icon--img" style="background-image: url(&quot;/img/icons/facebook.png&quot;)" title="facebook"></div>
                </a>
                <a class="footer__icon" href="{{ setting('site.yt') }}">
                    <div class="footer__icon--img" style="background-image: url(&quot;/img/icons/youtube.png&quot;)" title="youtube"></div>
                </a>
            </div>
            <div class="footer__item--txt">
                <a class="footer__txt--link" href="{{ setting('site.daria') }}">Левкович Дарья</a>
                <a class="footer__txt--link" href="{{ setting('site.karina') }}">Армидинова Карина</a>
                <p class="footer__txt">2020</p>
            </div>
        </div>
    </div>
</footer>

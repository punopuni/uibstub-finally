<header class="header">
    <div class="container">
        <div class="header__wrapper">
            <a class="header__logo" href="http://uib.kz/" target="_blank">
                <img src="/img/logo-uib.png">
            </a>
            @if(auth()->check())
            <div class="header__avatar" style="background-image: url({{ '/storage/'.$user_avatar }})">
                <a class="header__exit" href="/logout">Выход</a>
            </div>
            @endif
        </div>
    </div>
</header>

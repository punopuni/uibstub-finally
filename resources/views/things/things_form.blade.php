@extends('layouts.app')
@section('content')
    <body class="things">
    <div class="content">
        @include('layouts.main_menu')
        <div class="container">
            <div class="employees__title">Вещи UIB</div>
            <div class="map__wrapper">
                <nav role="tablist">
                    <div class="nav nav-tabs">
                        <div class="nav-link" id="defaultOpen" data-target="search">Пропажа</div>
                        <div class="nav-link" data-target="finding">Находка</div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane" id="search">
                            <div class="things__form--wrapper">
                                <p><span class="label-required">*</span>- обязательные поля</p>
                                <form action="/thinks_form" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="search-tel">Контакты для связи
                                            <span class="label-required">*</span>
                                        </label>
                                        <input class="form-control" id="search-tel" type="tel" placeholder="Телефон" required="required" name="phone">
                                    </div>
                                    <div class="form-group">
                                        <label for="search-tel-other">Допольнительный телефон</label>
                                        <input class="form-control" id="search-tel-other" type="tel" placeholder="Телефон" name="sec_phone">
                                    </div>
                                    <div class="form-group">
                                        <label for="search-textarea">Описание найденной вещи (где нашли, день/время, примечательные черты)
                                            <span class="label-required">*</span>
                                        </label>
                                        <textarea class="form-control" id="search-textarea" rows="3" required="required" name="disc"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="search-file">Фото</label>
                                        <input class="form-control-file" id="search-file" type="file" name="photo">
                                    </div>
                                    <input type="hidden" name="type" value="1">
                                    <button class="btn btn--default" type="submit">Отправить</button>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="finding">
                            <div class="things__form--wrapper">
                                <p><span class="label-required">*</span>- обязательные поля</p>
                                <form action="/thinks_form" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="search-tel">Контакты для связи
                                        <span class="label-required">*</span>
                                    </label>
                                    <input class="form-control" id="search-tel" type="tel" placeholder="Телефон" required="required" name="phone">
                                </div>
                                <div class="form-group">
                                    <label for="search-tel-other">Допольнительный телефон</label>
                                    <input class="form-control" id="search-tel-other" type="tel" placeholder="Телефон" name="sec_phone">
                                </div>
                                <div class="form-group">
                                    <label for="search-textarea">Описание найденной вещи (где нашли, день/время, примечательные черты)
                                        <span class="label-required">*</span>
                                    </label>
                                    <textarea class="form-control" id="search-textarea" rows="3" required="required" name="disc"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="search-file">Фото</label>
                                    <input class="form-control-file" id="search-file" type="file" name="photo">
                                </div>
                                    <input type="hidden" name="type" value="2">
                                    <button class="btn btn--default" type="submit">Отправить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
@endsection

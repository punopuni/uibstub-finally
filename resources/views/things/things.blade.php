@extends('layouts.app')
@section('content')
<body class="things-list">
<div class="content">
    @include('layouts.main_menu')
    <div class="container">
        <div class="employees__title">Вещи UIB</div>
        <div class="links">
            <div class="links__wrapper">
                <a class="links__item active" href="/things">Все</a>
                <a class="links__item" href="/thing/1">Пропажа</a>
                <a class="links__item" href="/thing/2">Находка</a>
            </div>
        </div>
        <div class="employees__wrapper">
            @foreach($things as $thing)
            <a class="employees__items" href="/thing_article/{{$thing->id}}">
                <div class="employees__items--bg">
                    <div class="news__wrapper">
                        <div class="news__date">{{ $thing->created_at }}</div>
                        @if($thing->type == 1)
                        <div class="news__author search">Пропажа</div>
                        @else
                        <div class="news__author find">Находка</div>
                        @endif
                    </div>
                    <div class="things-list__desc">{{ $thing->disc }}</div>
                </div>
            </a>
            @endforeach
            {{ $things->links() }}
        </div>
    </div>
</div>
@endsection

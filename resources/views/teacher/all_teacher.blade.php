@extends('layouts.app')
@section('content')
<body class="employees">
<div class="content">
    @include('layouts.main_menu')
    <div class="container">
        <div class="employees__title">Преподователи</div>
        <div class="links">
            <div class="links__wrapper">
                <a class="links__item active" href="/teacher">Все</a>
                @foreach($categories as $category)
                <a class="links__item" href="/teacher/{{ $category->id }}">{{ $category->name }}</a>
                @endforeach
            </div>
        </div>
        <div class="employees__wrapper">
            @foreach($teachers as $teacher)
            <a class="employees__items" href="/teacher_article/{{ $teacher->id }}">
                <div class="employees__items--bg">
                @if($teacher->photo)
                    <div class="employees__img" style="background-image: url({{ asset('/storage/' . $teacher->photo) }})"></div>
                @else
                    <div class="employees__img" style="background-image: url({{ asset('/img/no-photo.png') }})"></div>
                @endif
                    <div class="employees__name">{{$teacher->name}}</div>
                    <div class="employees__desc">{{$teacher->disc}}</div>
                </div>
            </a>
            @endforeach
            {{ $teachers->links() }}
        </div>
    </div>
</div>
@endsection

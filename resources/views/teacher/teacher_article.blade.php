@extends('layouts.app')
@section('content')
<body class="teacher">
<div class="content">
    @include('layouts.main_menu')
    <div class="container">
        <div class="personal-area__wrapper">
            <div class="personal-area__item">
                <div class="personal-area__ava--wrapper">
                    <div class="personal-area__ava" style="background-image: url({{ asset('/storage/' . $teacher->photo) }})"></div>
                </div>
                <div class="personal-area__text">
                    <h6 class="personal-area__name">{{$teacher->name}}</h6>
                    <div class="personal-area__info">
                        {!! $teacher->about !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')
@section('content')
<body class="map">
<div class="content">
    @include('layouts.main_menu')
    <div class="container">
        <div class="map__wrapper">
            <nav role="tablist">
                <div class="nav nav-tabs">
                    @foreach($floors as $floor)
                        @if($floor->id == 1)
                        <div class="nav-link" id="defaultOpen" data-target="{{$floor->id}}">{{$floor->name}}</div>
                        @else
                        <div class="nav-link" data-target="{{$floor->id}}">{{ $floor->name }}</div>
                        @endif
                    @endforeach
                </div>
                <div class="tab-content">
                    @foreach($floors as $floor)
                    <div class="tab-pane" id="{{$floor->id}}">
                        <div class="map__table">
                            <div class="map__table--items">
                                <div class="map__table--number">#</div>
                                <div class="map__table--desc">Описание</div>
                            </div>
                            @foreach($floor->map as $map)
                            <div class="map__table--items">
                                <div class="map__table--number">{{$map->number}}</div>
                                <div class="map__table--desc">{{$map->desc}}</div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>
            </nav>
        </div>
    </div>
</div>
@endsection

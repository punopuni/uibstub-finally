<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    protected $fillable = ['name', 'type'];

    public function news()
    {
        return $this->hasMany('App\News', 'news_category_id');
    }
}

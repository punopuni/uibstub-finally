<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedule';

    protected $fillable = ['time','audience', 'teacher', 'group_id', 'day'];

    public function group()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject', 'subject_id');
    }
}

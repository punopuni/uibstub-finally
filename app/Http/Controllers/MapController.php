<?php

namespace App\Http\Controllers;

use App\Floor;
use App\Map;
use Illuminate\Http\Request;

class MapController extends Controller
{
    public function index()
    {
        $vars = [
          'maps'    => Map::all(),
          'floors'  => Floor::all(),
        ];
        return view('map.map', $vars);
    }
}

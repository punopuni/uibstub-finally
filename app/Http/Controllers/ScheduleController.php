<?php

namespace App\Http\Controllers;

use App\Group;
use App\Schedule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ScheduleController extends Controller
{
    public function index()
    {
        /**
         * Обращаемся к группе куда передаем id текущего пользователя что бы через релейшены обратиться к нужному для
         * юзера расписанию
          **/
        return view('schedule.schedule', ['schedule' => Group::find(auth()->user()->group->id)->schedule()->get()]);
    }
}

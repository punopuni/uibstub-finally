<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PersonalAreaController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('personal_area.index', ['user' => $user]);
    }

    public function upload_photo(Request $request)
    {
        $user = auth()->user();
        $path = Storage::putFile('public/users', $request->file('photo'));

        $path_detail = explode('/', $path);

        $user->avatar = $path_detail[1] . '/' . $path_detail[2];
        $user->save();
        return redirect()->back();
    }
}

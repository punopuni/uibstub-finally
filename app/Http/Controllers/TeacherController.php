<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\TeacherCategory;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function index()
    {
        $vars = [
            'teachers'   => Teacher::paginate(8),
            'categories'  => TeacherCategory::all(),
        ];
        return view('teacher.all_teacher', $vars);
    }

    public function category_index($id)
    {
        $vars = [
          'teachers'    => TeacherCategory::find($id)->teacher()->paginate(8),
          'categories'  => TeacherCategory::all(),
          'category_id'   => $id,
        ];
        return view('teacher.category_teacher', $vars);
    }

    public function teacher_article($id)
    {
        $teacher = ['teacher' => Teacher::find($id)];
        return view('teacher.teacher_article', $teacher);
    }
}

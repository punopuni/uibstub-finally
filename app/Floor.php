<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $fillable = ['name'];

    public function map()
    {
        return $this->hasMany('App\Map', 'floor_id');
    }
}

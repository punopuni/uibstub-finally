<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['name', 'disc', 'text', 'author_id', 'news_category_id', 'type'];

    public function newscategory()
    {
        return $this->belongsTo('App\NewsCategory', 'news_category_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'author_id');
    }
}

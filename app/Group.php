<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name'];

    public function user()
    {
        return $this->hasMany('App\User', 'group_id');
    }

    public function schedule()
    {
        return $this->hasOne('App\Schedule', 'group_id');
    }
}
